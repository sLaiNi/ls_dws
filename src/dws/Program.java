package dws;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * @author ...
 *
 */
public class Program {

	public static void main(String[] args) {

		try {
			BufferedReader in
			= new BufferedReader(new FileReader("DWS-TOP-DIVIDENDE.csv"));

			// die ersten sechs Zeilen �berlesen
			int cnt = 0;
			while (in.readLine() != null && cnt < 7) {
				in.readLine();
				cnt++;
			}

			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy");
			String s;
			Date je = null;
			Date jb = null;
			String year = null;
			int jewert = 0;
			int jbwert = 0;
			try {
				je = sdf.parse("30.12.2011");
				jb = sdf.parse("01.01.2011");
				System.out.printf("Jahresende gesetzt: %tF%n", je);
				year = sdfToYear(je);
				System.out.println("Es werden die Eintr�ge gesucht f�r das Jahr: " + year);
			} catch (ParseException e) {
				e.printStackTrace();
			}  
			
			while ((s = in.readLine()) != null) {
				in.mark(1000);
				String nextLine = in.readLine();
				in.reset();
				String nextYear = nextLine.split(";")[0].split("\\.")[2];
				String[] ds = s.split(";"); //Splitte String am Trennzeichen ";" -> Erzeugt Array mit allen Daten
				Date d = null;
				String[] currentYear;
				try {
					d =sdf.parse(ds[0]);
					currentYear = ds[0].split("\\."); // Splitte das erhaltene Datum am Punkt -> [0] = Tag [1] = Monat [2] = Jahr
				} catch (ParseException e) {
					e.printStackTrace();
					break;
				}
				if (je.equals(d)) {
					System.out.printf("gefunden %tF%n", d);
					jewert = Integer.parseInt(s.split("[;\\,]")[1]);
					System.out.println("Startwert des letzten Tages: " + jewert);
				}
				if(jb.equals(d) || Integer.valueOf(year) >  Integer.valueOf(nextYear)) {
					System.out.printf("Erster Wert des Jahres gefunden: %tF%n", d);
					jbwert = Integer.parseInt(s.split("[;\\,]")[1]);
					System.out.println("Startwert des ersten Tages: " + jbwert);
					break;
				}
			}
			
			System.out.println("\nErgebnis: ");
			if(jewert > jbwert) {
				System.out.println("bull");
			} else {
				System.out.println("bear");
			}
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static String sdfToYear(Date _date)
	{
		SimpleDateFormat yearsdf = new SimpleDateFormat("yyyy");
		String year = yearsdf.format(_date);
		return year;
	}
}

